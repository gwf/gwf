.PHONY: build

dep:
	@go get

build:
	@go build -o build/gwf main.go convert.go component.go

install:
	@go install

uninstall:
	@rm (which gwf)

generate:
	go generate example/content/IndexComponent.go
	go generate example/content/app/AppComponent.go