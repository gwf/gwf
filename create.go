package main

import (
	"fmt"
	"gitlab.com/gwf/gwf/projectparser"
	"go/ast"
	"go/token"
	"os"
	"path/filepath"
)

// TODO: Add to application/module

// Create new files for a component. Expects the path to exist but
func createComponent(name, p string, withInterception bool) error {
	p, err := filepath.Abs(p)
	if err != nil {
		return fmt.Errorf("failed to get absolute directory path from 'path' parameter: %w", err)
	}
	if err = os.MkdirAll(p, os.ModePerm); err != nil && !os.IsExist(err) {
		return fmt.Errorf("failed to create path to components: %w", err)
	}
	_, pckg := filepath.Split(p)
	return crtComponent(name, p, pckg, withInterception)
}

func createWirer(args []string, p string) error {
	/*
		TODO:
			- Get project import path from go.mod
			- get components to import from main.go -> Application.Components
			- scan components for Constructors/struct initializers
			- generate wire_gwf_gen.go file containing the Wirer function
	*/
	absWorkDir, err := filepath.Abs(p)
	if err != nil {
		return fmt.Errorf("failed to build absolute path to working directory: %w", err)
	}

	return crtWirer(args, absWorkDir)
}

func doOnComponentList(f *ast.File, action func(comp *ast.CompositeLit) error) error {
	for _, decl := range f.Decls {
		gen, ok := decl.(*ast.GenDecl)
		if !ok {
			continue
		}
		if gen.Tok != token.VAR {
			continue
		}
		for _, spec := range gen.Specs {
			part, ok := spec.(*ast.ValueSpec)
			if !ok {
				continue
			}
			if len(part.Names) != 1 {
				continue
			}
			if len(part.Values) != 1 {
				continue
			}
			if part.Names[0].Name != "Application" {
				continue
			}
			comp, ok := part.Values[0].(*ast.CompositeLit)
			if !ok {
				continue
			}
			for _, element := range comp.Elts {
				keyVal, ok := element.(*ast.KeyValueExpr)
				if !ok {
					continue
				}
				keyIdent, ok := keyVal.Key.(*ast.Ident)
				if !ok {
					continue
				}
				if projectparser.ToString(keyIdent) != "Components" {
					continue
				}
				comp, ok = keyVal.Value.(*ast.CompositeLit)
				if !ok {
					continue
				}
				err := action(comp)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}
