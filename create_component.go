package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

const (
	goComponentTempl = `package {{ .Package }}

//go:generate gwf gen component {{ .Name }}Component
// {{ .Name }} component generated by gwf.
type {{ .Name }}Component struct {
	// TODO: Insert fields here
}

// TODO: Add functions which can be accessed by the template

{{ if .WithInterception -}}
func (comp *{{ .Name }}Component) Intercept(ctx *lib.RenderContext, templ string) (string, error) {
	// TODO: Alter the template with the given context
	return templ, nil
}
{{- end }}`
	gohtmlComponentTempl = `<p>{{ .Name }}Component works!</p>`
)

type comp struct {
	// Package of the component to create into.
	Package string
	// Name of the component (without 'Component') in it's name.
	Name string
	// WithInterception signals if the component implements the InterceptionComponent interface.
	WithInterception bool
}

// Create the files from templates
func crtComponent(name, p, pckg string, withInterception bool) error {
	component := &comp{
		Package:          pckg,
		Name:             name,
		WithInterception: withInterception,
	}
	var (
		goFile   = filepath.Join(p, fmt.Sprintf("%sComponent.go", name))
		htmlFile = filepath.Join(p, fmt.Sprintf("%sComponent.gohtml", name))
	)

	// Parse templates first
	gofileTemplate, err := template.New(name).Parse(goComponentTempl)
	if err != nil {
		return fmt.Errorf("failed to parse Go File ComponentTemplate: %w", err)
	}
	gohtmlComponentTempl, err := template.New(name).Parse(gohtmlComponentTempl)
	if err != nil {
		return fmt.Errorf("failed to parse Go HTML File ComponentTemplate: %w", err)
	}

	// Execute templates
	var gofileRaw, gohtmlfileRaw bytes.Buffer
	err = gofileTemplate.Execute(&gofileRaw, component)
	if err != nil {
		return fmt.Errorf("failed to execute Go File template on %v: %w", component, err)
	}
	err = gohtmlComponentTempl.Execute(&gohtmlfileRaw, component)
	if err != nil {
		return fmt.Errorf("failed to execute HTML File template on %v: %w", component, err)
	}

	// Write files to disc
	err = ioutil.WriteFile(goFile, gofileRaw.Bytes(), os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to write Go File: %w", err)
	}
	err = ioutil.WriteFile(htmlFile, gohtmlfileRaw.Bytes(), os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to write html File: %w", err)
	}

	// Run goimports
	cmd := exec.Command("goimports", "-w", goFile)
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to run goimports on generated gofile: %w", err)
	}

	log.Printf("Successfully generated %sComponent!", name)
	return nil
}
