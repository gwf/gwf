package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
)

// genComponent expects a valid name and a absolute directory path to where to do that.
func genComponent(name, p string) error {
	// Expecting name and workingDir to be valid
	absWorkDir, err := filepath.Abs(p)
	if err != nil {
		return fmt.Errorf("failed to get absolute directory path from 'path' parameter: %w", err)
	}

	// Check for file exists
	_, err = os.Stat(absWorkDir)
	if os.IsNotExist(err) {
		return err
	}

	// Open gofile containing the component
	goFilePath := path.Join(absWorkDir, fmt.Sprintf("%s.go", name))
	goFile, err := os.Lstat(goFilePath)
	if err != nil {
		return fmt.Errorf("failed to get stat of goFile %s: %w", goFilePath, err)
	}

	// Get info of template file
	templFilePath := path.Join(absWorkDir, fmt.Sprintf("%s.gohtml", name))
	templFile, err := os.Lstat(templFilePath)
	if err != nil {
		return fmt.Errorf("failed to get stat of templateFile %s: %w", templFilePath, err)
	}

	spec := &componentSpec{
		goFilePath:    goFilePath,
		templFilePath: templFilePath,

		goFile:       goFile,
		templateFile: templFile,
	}

	// Add additional file with code from gwf
	b, err := generateComponent(os.Args[1:], name, spec)
	if err != nil {
		return fmt.Errorf("failed to generate code for component: %w", err)
	}

	goGenFile := path.Join(absWorkDir, fmt.Sprintf("%s.gen.go", name))
	f, err := os.Create(goGenFile)
	if err != nil {
		return fmt.Errorf("failed to open raw generated file: %w", err)
	}

	// Write to file
	_, err = f.WriteString(b)
	if err != nil {
		return fmt.Errorf("failed to write formatted code to file: %w", err)
	}
	_ = f.Close()

	// Run goimports tool :: formats and organizes imports as well as delete unused ones
	cmd := exec.Command("goimports", "-w", goGenFile)
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("error while running goimports: %w", err)
	}

	log.Printf("successfully generated '%s'\n", name)
	return nil
}
