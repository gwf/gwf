module gitlab.com/gwf/gwf

go 1.13

require (
	github.com/google/wire v0.4.0
	github.com/urfave/cli/v2 v2.1.0
	gitlab.com/gwf/lib v0.0.0-20191227112445-973d29c3431b // indirect
	golang.org/x/tools v0.0.0-20191220234730-f13409bbebaf // indirect
)
