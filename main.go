package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"os"
)

/* Workflow
1. A component file containig a comment `//go:generate gwf component -name=<name>Component`
2. Search for golang file `<name>Component.go` and `<name>Component.gohtml`
3. Generate glue code to load the template and provide functions needed by the `gwf build` command
4. Convert every occurrence of `{{ component "<name>" <param1> <param2> ... }}` to `{{ template "<name>" wrap "<name>" <param1> <param2> ... }}`
6. `gwf component` or `go generate` should generate code in the same directory.
7. `gwf build` should build a single executable file containing all templates and logic code.
	- `gwf build` should build a single executable file which:
		1. contains a main()-function serving the application
		2. contains all logical go and template code
		3. the global template should contain a wrap function which takes parameters and build an instance of the desired Component from them.
*/

type componentSpec struct {
	goFilePath, templFilePath string

	goFile, templateFile os.FileInfo
}

func main() {
	// Build the application information. Actions are just retrieving the prameters and pass them to the actual functions to check them.
	app := &cli.App{
		Name:  "gwf",
		Usage: "command line tool to initialize a gwf project and generate parts of a gwf application",
		Commands: []*cli.Command{
			{
				Name:    "generate",
				Aliases: []string{"gen", "g"},
				Usage:   "generate a new part of the application",
				Subcommands: []*cli.Command{
					{
						Name:    "component",
						Aliases: []string{"comp", "c"},
						Usage:   `use the existing <name>.go and <name>.gohtml files to generate the code for a component. To create the files for a new component use 'go init'`,
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:     "path",
								Required: false,
								Usage:    "relative path to the component files",
								Value:    ".",
							},
						},
						Action: func(c *cli.Context) error {
							if c.NArg() < 1 {
								return fmt.Errorf("couldn't generate component. Missing name")
							}
							name := c.Args().Get(0)
							p := c.String("path")
							return genComponent(name, p)
						},
					},
				},
			}, {
				Name:    "create",
				Aliases: []string{"c"},
				Usage:   "create new parts of a gwf application",
				Subcommands: []*cli.Command{
					{
						Name:    "component",
						Aliases: []string{"comp", "c"},
						Usage:   "create the files necessary for a new component",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:    "path",
								Aliases: []string{"p"},
								Usage:   "relative directory path in which the component should be created",
								Value:   ".",
							}, &cli.BoolFlag{
								Name:    "with-interception",
								Aliases: []string{"intercept", "i"},
								Usage:   "flag if the component should implement the InterceptionComponent interface",
								Value:   false,
							},
						},
						Action: func(c *cli.Context) error {
							if c.NArg() < 1 {
								return fmt.Errorf("couldn't create component. Missing name arg")
							}
							name := c.Args().Get(0)
							p := c.String("path")
							withInterception := c.Bool("with-interception")
							return createComponent(name, p, withInterception)
						},
					}, {
						Name:    "wirer",
						Aliases: []string{"wire", "w"},
						Usage:   "create files for dependency injection with wire",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:    "path",
								Aliases: []string{"p"},
								Usage:   "relative directory path in which the component should be created",
								Value:   ".",
							},
						},
						Action: func(c *cli.Context) error {
							return createWirer(os.Args, c.String("path"))
						},
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatalf("failed to run gwf: %v", err)
	}

	/*
		gwf serve
		TODO
			- Build project
			- Serve application
			- Livereload

		gwf create
		TODO
			- Create new parts of the application
			- Create Module

		gwf generate
		TODO
			- update module file
			- update component.gen.go file

		gwf build
		TODO
			- generate Wirer: Scan whole application, build Injectors and map templateid to injectors. Then run wire
			- build whole application to single executable

		gwf init
		TODO generate new project from template
			- Clone template git repo
			- Initialize new git repo
			- run code generation (wire function and injectors)
	*/
}
