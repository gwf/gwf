package projectparser

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
)

var (
	modulePathRegex         = regexp.MustCompile("(?m)^module (.*)$")
	moduleDependenciesRegex = regexp.MustCompile("(?m)require \\(((\\s|.)*)\\)")
)

type ProjectParser struct {
	// Absolute Path to the project directory.
	ProjectRoot string

	// Packages contains information about every package used in the project if internal or external.
	Packages []*PackageDependency
}

func New(projectRootPath string) (*ProjectParser, error) {
	p, err := filepath.Abs(projectRootPath)
	if err != nil {
		return nil, fmt.Errorf("failed to get absolute path of project: %w", err)
	}
	return &ProjectParser{
		ProjectRoot: p,
	}, nil
}

func ParseComponents(gofilepath string) (map[string]*CompParseResult, error) {
	root, _ := filepath.Split(gofilepath)
	pp, err := New(root)
	if err != nil {
		return nil, err
	}

	_, pckg := filepath.Split(root[:len(root)-1])

	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, gofilepath, nil, 0)
	if err != nil {
		return nil, err
	}
	return pp.ParseComponents(&PackageDependency{Path: pckg}, f)
}

type GoMod struct {
	Path             string
	ModuleDependency map[string]string
}

// GetModule returns the name of the module as stated in the `go.mod` file.
func (pp *ProjectParser) GetModule() (*GoMod, error) {
	// read project path
	goModFile, err := ioutil.ReadFile(filepath.Join(pp.ProjectRoot, "go.mod"))
	if err != nil {
		return nil, fmt.Errorf("failed to read go module file: %w", err)
	}
	submatch := modulePathRegex.FindStringSubmatch(string(goModFile))
	if len(submatch) != 2 {
		return nil, fmt.Errorf("match is invalid: '%v'", submatch)
	}

	// read project dependencies
	dependencyMatch := moduleDependenciesRegex.FindStringSubmatch(string(goModFile))
	if len(dependencyMatch) != 3 {
		return nil, fmt.Errorf("match is invalid: '%s'", dependencyMatch)
	}
	modDep := make(map[string]string)
	for _, line := range strings.Split(strings.TrimSpace(dependencyMatch[1]), "\n") {
		if line == "" {
			continue
		}
		split := strings.Split(strings.TrimSpace(line), " ")
		// Length should be multiple of 2 to be valid
		if len(split)%2 != 0 {
			log.Fatalf("Invalid go.mod format: %s", split)
		}
		for i := 0; i < len(split); i += 2 {
			modDep[split[i]] = fmt.Sprintf("%s@%s", split[i], split[i+1])
		}
	}

	return &GoMod{
		Path:             submatch[1],
		ModuleDependency: modDep,
	}, nil
}

type ProjectConfig struct {
	Initializers []*ast.FuncDecl
}

func (cfg *ProjectConfig) GetInitializerFor(typ ast.Expr) *ast.FuncDecl {
	var initializer *ast.FuncDecl
	for _, init := range cfg.Initializers {
		for _, ret := range init.Type.Results.List {
			// Remove everything before and including the dot
			retType := ToString(ret.Type)
			dotInd := strings.Index(retType, ".")
			retType = retType[dotInd+1:]

			// Remove star
			typ := strings.Replace(ToString(typ), "*", "", 1)

			if retType == typ {
				return init
			}
		}
	}
	return initializer
}

func (cfg *ProjectConfig) String() string {
	var b bytes.Buffer
	b.WriteRune('[')
	for i, init := range cfg.Initializers {
		b.WriteString(init.Name.Name)
		if i < len(cfg.Initializers)-1 {
			b.WriteString(", ")
		}
	}
	b.WriteRune(']')
	return b.String()
}

type ProjectParseResult struct {
	Config *ProjectConfig

	Components map[string]*CompParseResult
	Classes    map[string]*ClassParseResult
}

func NewParseResult() *ProjectParseResult {
	return &ProjectParseResult{
		Components: make(map[string]*CompParseResult),
		Classes:    make(map[string]*ClassParseResult),
	}
}

func (pp *ProjectParser) Parse(res *ProjectParseResult) error {
	/*
		TODO: Dependencies are mainly passed to wire.
			- gwf configuration file (gwf_config.go) - Custom Initializers/Providers
			- Recursively iterate over "content" directory and parse files
	*/
	cfg, err := pp.parseConfig()
	if err != nil {
		return err
	}
	res.Config = cfg

	// Parse components referenced in the main.go file
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, path.Join(pp.ProjectRoot, "main.go"), nil, 0)
	if err != nil {
		return fmt.Errorf("failed to parse main.go: %w", err)
	}
	var pckgDep []*PackageDependency
	for _, imp := range f.Imports {
		p := strings.ReplaceAll(imp.Path.Value, `"`, "")
		dep := &PackageDependency{Path: p}
		if imp.Name != nil {
			dep.AltName = imp.Name.Name
		}
		pckgDep = append(pckgDep, dep)
	}

	// TODO: Parse only dependencies referenced in main.go -> Application -> Components
	var compPckgs []*PackageDependency
	compList := getComponentList(f)
	for _, comp := range compList {
		for _, elt := range comp.Elts {
			sel, ok := elt.(*ast.SelectorExpr)
			if !ok {
				return fmt.Errorf("unsupported component expression of type '%v': %v", reflect.TypeOf(elt), elt)
			}
			pckgName, ok := sel.X.(*ast.Ident)
			if !ok {
				return fmt.Errorf("unsupported selector expression of type '%v': %v", reflect.TypeOf(sel.X), sel.X)
			}
			found := false
			for _, dep := range pckgDep {
				if dep.Name() == pckgName.Name {
					compPckgs = append(compPckgs, dep)
					found = true
					break
				}
			}
			if !found {
				return fmt.Errorf("couldn't find package dependency for %s", pckgName)
			}
		}
	}

	err = pp.ParsePackages(res, compPckgs)
	if err != nil {
		return fmt.Errorf("failed to parse component packages: %w", err)
	}

	return nil
}

func getComponentList(f *ast.File) []*ast.CompositeLit {
	var compList []*ast.CompositeLit
	for _, decl := range f.Decls {
		gen, ok := decl.(*ast.GenDecl)
		if !ok {
			continue
		}
		if gen.Tok != token.VAR {
			continue
		}
		for _, spec := range gen.Specs {
			part, ok := spec.(*ast.ValueSpec)
			if !ok {
				continue
			}
			if len(part.Names) != 1 {
				continue
			}
			if len(part.Values) != 1 {
				continue
			}
			if part.Names[0].Name != "Application" {
				continue
			}
			comp, ok := part.Values[0].(*ast.CompositeLit)
			if !ok {
				continue
			}
			for _, element := range comp.Elts {
				keyVal, ok := element.(*ast.KeyValueExpr)
				if !ok {
					continue
				}
				keyIdent, ok := keyVal.Key.(*ast.Ident)
				if !ok {
					continue
				}
				if ToString(keyIdent) != "Components" {
					continue
				}
				comp, ok = keyVal.Value.(*ast.CompositeLit)
				if !ok {
					continue
				}
				compList = append(compList, comp)
			}
		}
	}
	return compList
}

func (pp *ProjectParser) ParsePackages(res *ProjectParseResult, pckgs []*PackageDependency) error {
	module, err := pp.GetModule()
	if err != nil {
		return err
	}

	for _, pckg := range pckgs {
		dir, err := pckg.AbsolutePath(module)
		if err != nil {
			return err
		}

		files, err := ioutil.ReadDir(dir)
		if err != nil {
			return err
		}

		err = pp.parseFile(res, pckg, dir, files)
		if err != nil {
			return err
		}
	}
	return nil
}

func (pp *ProjectParser) recursiveParse(res *ProjectParseResult, pckgPath *PackageDependency, directory string) error {
	contentFiles, err := ioutil.ReadDir(directory)
	if err != nil {
		return err
	}
	err = pp.parseFile(res, pckgPath, directory, contentFiles)
	if err != nil {
		return err
	}

	// Recursive
	subdirs, err := ioutil.ReadDir(directory)
	if err != nil {
		return err
	}
	for _, subdir := range subdirs {
		if subdir.IsDir() {
			err := pp.recursiveParse(res, &PackageDependency{Path: path.Join(pckgPath.Path, subdir.Name())}, filepath.Join(directory, subdir.Name()))
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (pp *ProjectParser) parseFile(res *ProjectParseResult, pckgPath *PackageDependency, directory string, files []os.FileInfo) error {
	pp.Packages = append(pp.Packages, pckgPath)
	for _, f := range files {
		if !strings.HasSuffix(f.Name(), ".go") {
			continue
		}
		fset := token.NewFileSet()
		parsedFile, err := parser.ParseFile(fset, filepath.Join(directory, f.Name()), nil, 0)
		if err != nil {
			return fmt.Errorf("failed to parse dir %s: %w", pp.ProjectRoot, err)
		}
		if strings.HasSuffix(f.Name(), "Component.go") {
			comps, err := pp.ParseComponents(pckgPath, parsedFile)
			if err != nil {
				return fmt.Errorf("failed to parse components: %w", err)
			}
			for name, val := range comps {
				_, present := res.Components[name]
				if present {
					return fmt.Errorf("duplicate component: %s", name)
				}
				res.Components[name] = val
			}
		} else if strings.HasSuffix(f.Name(), ".go") {
			classes, err := pp.parseClasses(pckgPath, parsedFile)
			if err != nil {
				return fmt.Errorf("failed to parse classes: %w", err)
			}
			for name, val := range classes {
				_, present := res.Classes[name]
				if present {
					return fmt.Errorf("duplicate component: %s", name)
				}
				res.Classes[name] = val
			}
		}
	}
	return nil
}

func (pp *ProjectParser) parseConfig() (*ProjectConfig, error) {
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, filepath.Join(pp.ProjectRoot, "gwf_config.go"), nil, 0)
	if err != nil {
		return nil, fmt.Errorf("failed to parse gwf_config.go file: %w", err)
	}

	var inits []*ast.FuncDecl
	for _, decl := range f.Decls {
		fun, ok := decl.(*ast.FuncDecl)
		if !ok {
			continue
		}

		inits = append(inits, fun)
	}

	return &ProjectConfig{Initializers: inits}, nil
}

func (pp *ProjectParser) parseClasses(pckgPath *PackageDependency, f *ast.File) (map[string]*ClassParseResult, error) {
	// Parse imports
	imports := make(map[string]*ast.ImportSpec)
	for _, imp := range f.Imports {
		if imp.Name != nil {
			imports[imp.Name.Name] = imp
		} else {
			_, name := filepath.Split(imp.Path.Value)
			imports[name] = imp
		}
	}

	res := make(map[string]*ClassParseResult)
	structs := pp.parseStructs(f.Decls)
	for name, str := range structs {
		methods := make(map[string]*ast.FuncDecl)
		var constructor *ast.FuncDecl
	declLoop:
		for _, dec := range f.Decls {
			// Try to parse function declarations
			f, ok := dec.(*ast.FuncDecl)
			if !ok {
				continue
			}
			// Check if New function has any dependencies handled by wire
			if f.Name.Name == fmt.Sprintf("New%s", name) || f.Name.Name == "New" {
				for _, ret := range f.Type.Results.List {
					if matches, err := regexp.MatchString(`\*?`+name, ToString(ret.Type)); err != nil {
						return nil, fmt.Errorf("failed to parse matching string for constructor function return: %v", err)
					} else if !matches {
						continue declLoop
					}
				}
				err := checkConstructor(name, f)
				if err != nil {
					return nil, fmt.Errorf("checking constructor of class %s failed: %w", name, err)
				}
				// Duplicates are not allowed as golang doesn't support overloading
				if constructor != nil {
					return nil, fmt.Errorf("only one constructor allowed, found multiple for %s", name)
				}
				constructor = f
				continue
			}

			if f.Recv == nil {
				continue
			}

			// Check if receiver is the struct
			isMethod := false
			for _, recv := range f.Recv.List {
				star, ok := recv.Type.(*ast.StarExpr)
				if !ok {
					continue
				}
				iden, ok := star.X.(*ast.Ident)
				if !ok {
					continue
				}
				if iden.Name != name {
					continue
				}
				isMethod = true
				break
			}
			if isMethod {
				methods[f.Name.Name] = f
			}
		}

		res[name] = &ClassParseResult{
			StructParseResult: &StructParseResult{
				Name:   name,
				Fields: str.Fields,
			},
			Imports:     imports,
			Package:     pckgPath,
			Methods:     methods,
			Constructor: constructor,
		}
	}
	return res, nil
}

func (pp *ProjectParser) ParseComponents(pckgPath *PackageDependency, f *ast.File) (map[string]*CompParseResult, error) {
	res := make(map[string]*CompParseResult)

	classes, err := pp.parseClasses(pckgPath, f)
	if err != nil {
		return nil, fmt.Errorf("failed to parse component class: %w", err)
	}

	for name, class := range classes {
		var hasInterceptor bool

		if !strings.HasSuffix(name, "Component") {
			continue
		}

		for fName, f := range class.Methods {
			// Try to parse as Interceptor
			if "Intercept" == fName {
				for _, recv := range f.Recv.List {
					if matches, err := regexp.MatchString(`\*?`+name, ToString(recv.Type)); err != nil {
						return nil, fmt.Errorf("failed to parse matching string for intercept function receiver: %v", err)
					} else if matches {
						// Found an Interceptor
						hasInterceptor = true
						break
					}
				}
				continue
			}
			// Check if Render function would be overwritten
			if fName == "Render" {
				for _, recv := range f.Recv.List {
					if matches, err := regexp.MatchString(`\*?`+name, ToString(recv.Type)); err != nil {
						return nil, fmt.Errorf("failed to parse matching string for render function receiver: %v", err)
					} else if matches {
						return nil, fmt.Errorf("'Render' function of %s would be overwritten. Please rename", name)
					}
				}
				continue
			}
			// Check if Init function would be overwritten
			if fName == "Init" {
				for _, recv := range f.Recv.List {
					if matches, err := regexp.MatchString(fmt.Sprintf("*?%s", name), ToString(recv.Type)); err != nil {
						return nil, fmt.Errorf("failed to parse matching string for init function receiver: %v", err)
					} else if matches {
						return nil, fmt.Errorf("'Init' function of %s would be overwritten. Please rename", name)
					}
				}
				continue
			}
		}

		res[name] = &CompParseResult{
			ClassParseResult: class,
			HasInterceptor:   hasInterceptor,
		}
	}
	return res, nil
}

// Parses a struct declaration and converts it to its params to generate a struct initializer.
func (pp *ProjectParser) parseStructs(decs []ast.Decl) map[string]*StructParseResult {
	res := make(map[string]*StructParseResult)
	for _, dec := range decs {
		gen, ok := dec.(*ast.GenDecl)
		if !ok {
			continue
		}
		// Iterate found Specs in declaration
		for _, spec := range gen.Specs {
			fields := make(map[string]ast.Expr)
			//fmt.Printf("\tSpec %v :: %+v\n", reflect.TypeOf(spec), spec)
			// Only Type declarations
			t, ok := spec.(*ast.TypeSpec)
			if !ok {
				continue
			}

			//fmt.Printf("\t\tTypeSpec %s :: %v %+v\n", t.Name, reflect.TypeOf(t.Type), t.Type)

			// Only struct types
			str, ok := t.Type.(*ast.StructType)
			if !ok {
				continue
			}
			// Iterate fields
			for _, field := range str.Fields.List {
				//fmt.Printf("\t\t\tField %v Type { %v %+v } :: %+v\n", field.Names, reflect.TypeOf(field.Type), field.Type, field)
				for _, name := range field.Names {
					fields[name.Name] = field.Type
				}
			}
			res[t.Name.Name] = &StructParseResult{
				Name:   t.Name.Name,
				Fields: fields,
			}
		}
	}
	return res
}

// Parses the `New` function building the component and returns the parameters it takes
// to remove them from the required fields in the init function
func checkConstructor(name string, f *ast.FuncDecl) error {
	if f.Recv != nil && len(f.Recv.List) != 0 {
		return fmt.Errorf("constructor `New` of `%s` is not allowed to have a receiver", name)
	}
	if f.Type.Results == nil || len(f.Type.Results.List) > 2 || len(f.Type.Results.List) == 0 {
		return fmt.Errorf("constructor `New` of `%s` can only return an instance of the component and an optional error", name)
	}
	if len(f.Type.Results.List) >= 1 {
		if matches, err := regexp.MatchString(fmt.Sprintf("\\*?%s", name), ToString(f.Type.Results.List[0].Type)); err != nil {
			return fmt.Errorf("failed to match return type for `%s`: %v", name, err)
		} else {
			if !matches {
				return fmt.Errorf("first return value of constructor has to be an instance of the component `%s`", name)
			}
		}
	}
	return nil
}

func ToString(expr ast.Expr) string {
	t := fmt.Sprintf("%s", expr)
	switch o := expr.(type) {
	case *ast.Ident:
		t = o.Name
	case *ast.SelectorExpr:
		t = fmt.Sprintf("%s.%s", o.X, o.Sel)
	case *ast.StarExpr:
		t = fmt.Sprintf("*%s", ToString(o.X))
	default:
		log.Fatalf("unsupported field type: %v", reflect.TypeOf(expr))
	}
	return t
}
