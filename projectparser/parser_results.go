package projectparser

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/token"
	"log"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"strings"
)

type ImportParseResult map[string]*ast.ImportSpec

func (imp ImportParseResult) String() string {
	var b bytes.Buffer
	for name, p := range imp {
		var res string
		if _, pLast := filepath.Split(p.Path.Value); pLast == name {
			res = fmt.Sprintf("%s\n", p.Path.Value)
		} else {
			res = fmt.Sprintf("%s %s\n", name, p.Path.Value)
		}
		_, err := fmt.Fprint(&b, res)
		if err != nil {
			log.Fatalf("failed to write to buffer: %v", err)
		}
	}
	return b.String()
}

type StructParseResult struct {
	Name string
	// Fields maps the name of the field to its Type as string representation.
	Fields map[string]ast.Expr
}

type ClassParseResult struct {
	*StructParseResult

	Imports     ImportParseResult
	Package     *PackageDependency
	Methods     map[string]*ast.FuncDecl
	Constructor *ast.FuncDecl
}

func (class *ClassParseResult) Name() string {
	return class.StructParseResult.Name
}

func (class *ClassParseResult) Fields() map[string]ast.Expr {
	return class.StructParseResult.Fields
}

func (class *ClassParseResult) PackageName() string {
	_, name := path.Split(class.Package.Path)
	return name
}

type CompParseResult struct {
	*ClassParseResult

	HasInterceptor bool
	TemplateFile   *os.FileInfo
}

func (comp *CompParseResult) Dependencies() []*Dependency {
	var res []*Dependency

	if comp.Constructor == nil {
		return res
	}

	for _, param := range comp.Constructor.Type.Params.List {
		// Names doesn't matter, order does
		for range param.Names {
			imp := comp.toImportSpec(param.Type)
			res = append(res, &Dependency{
				Type:   param.Type,
				Import: imp,
			})
		}
	}
	return res
}

func (comp *CompParseResult) toImportSpec(typ ast.Expr) *ast.ImportSpec {
	// Get import matching
	var imp *ast.ImportSpec
	switch p := typ.(type) {
	case *ast.SelectorExpr:
		i, ok := comp.Imports[p.Sel.Name]
		if !ok {
			log.Fatalf("import of component not present: %s. Imports: %+v", p.Sel.Name, comp.Imports)
		}
		imp = i
	case *ast.Ident:
		imp = &ast.ImportSpec{
			Path: &ast.BasicLit{
				Kind:  token.STRING,
				Value: comp.Package.String(),
			},
		}
	case *ast.StarExpr:
		imp = comp.toImportSpec(p.X)
	default:
		log.Fatalf("unsupported parameter ast type: %v", reflect.TypeOf(typ))
	}
	return imp
}

func (comp *CompParseResult) Fields() map[string]ast.Expr {
	res := make(map[string]ast.Expr)
	deps := comp.Dependencies()
	for name, field := range comp.StructParseResult.Fields {
		for _, dep := range deps {
			if ToString(dep.Type) != ToString(field) {
				res[name] = field
			}
		}
	}
	return res
}

func (comp *CompParseResult) FieldStrings() map[string]string {
	res := make(map[string]string)
	for name, field := range comp.Fields() {
		res[name] = ToString(field)
	}
	return res
}

type PackageDependency struct {
	Path    string
	AltName string
}

func (dep *PackageDependency) Name() string {
	var name string
	if dep.AltName == "" {
		_, name = path.Split(dep.Path)
	} else {
		name = dep.AltName
	}
	return strings.ReplaceAll(name, `"`, "")
}

func (dep *PackageDependency) AbsolutePath(gomod *GoMod) (string, error) {
	abs := strings.ReplaceAll(dep.Path, `"`, "")
	if strings.Contains(abs, gomod.Path) {
		// Search local
		abs = strings.ReplaceAll(abs, gomod.Path, "")
		if strings.HasPrefix(abs, "/") {
			abs = abs[1:]
		}
	} else {
		found := false
		for p, val := range gomod.ModuleDependency {
			if strings.Contains(abs, p) {
				abs = strings.Replace(abs, p, val, 1)
				found = true
				break
			}
		}
		if found {
			// Search on $GOPATH
			abs = filepath.Join(os.Getenv("GOPATH"), "pkg", "mod", abs)
		} else {
			// Check if reference to some core library
			absolute, err := filepath.Abs(filepath.Join(os.Getenv("GOROOT"), "src", abs))
			if err != nil {
				return "", err
			}
			_, err = os.Stat(absolute)
			if err != nil && !os.IsNotExist(err) {
				abs = filepath.Join(os.Getenv("GOPATH"), "src", abs)
			} else {
				abs = absolute
			}
		}
	}
	return filepath.Abs(abs)
}

func (dep *PackageDependency) String() string {
	var ret string
	if dep.AltName == "" {
		ret = dep.Path
	} else {
		ret = fmt.Sprintf(`%s %s`, dep.AltName, dep.Path)
	}
	return ret
}

type Dependency struct {
	Type   ast.Expr
	Import *ast.ImportSpec
}

func (dep *Dependency) ImportName() string {
	if dep.Import == nil {
		return ""
	}

	if dep.Import.Name != nil {
		return dep.Import.Name.Name
	} else {
		_, name := filepath.Split(dep.Import.Path.Value)
		return name
	}
}

func (dep *Dependency) ImportString() string {
	if dep.Import == nil {
		return ""
	}

	if dep.Import.Name != nil {
		return fmt.Sprintf("%s %s", dep.Import.Name.Name, dep.Import.Path.Value)
	} else {
		return dep.Import.Path.Value
	}
}
